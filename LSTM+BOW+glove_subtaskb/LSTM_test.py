#Author: Andraž P.

from preprocess import preprocess_data, tags_to_idx
import pandas as pd
import os
from keras.utils import to_categorical

def preprocess_test():
    path = os.path.join(os.path.dirname(__file__), os.pardir)
    train_file = "/training-v1/offenseval-training-v1-train_split.tsv"
    #val_file = "/training-v1/offenseval-training-v1-val_split.tsv"

    train_data = pd.read_csv(path + train_file, sep="\t")
    #val_data = pd.read_csv(path + val_file, sep="\t")

    print("Preprocessing data")
    label = 'subtask_b'
    xtrain, ytrain = preprocess_data(train_data, label)
    ytrain = tags_to_idx(ytrain)
    ytrain = to_categorical(ytrain, num_classes=2)
    print("ytrain hot encoded")
    print(ytrain)
    
    #xval, yval = preprocess_data(val_data, label)
    #yval = tags_to_idx(yval, idx=idx)
    #yval = to_categorical(yval, num_classes=2)
    #print("yval hot-encoded")
    #print(yval)


if __name__ == "__main__":
    preprocess_test()


