#Author: Andraž P.

import os
import copy
import pandas as pd
from model import build_model
from features import build_features
import pickle
from sklearn.base import BaseEstimator, TransformerMixin
from features import build_features
from train import TextCol
from keras.preprocessing.sequence import pad_sequences
import numpy as np
import argparse
import sys


#class text_col(BaseEstimator, TransformerMixin):
#    def __init__(self, key):
#        self.key = key

#   def fit(self, x, y=None):
#        return self

#    def transform(self, data_dict):
#        return data_dict[self.key]


def predict():
    """Runs classification with the trained model on input data."""

    parser = argparse.ArgumentParser()

    parser.add_argument("-data_path",
                        default="test_data_subtaskb/testset-taskb.tsv",
                        type=str,
                        help="The path to the data instances to be clasified. The dataset should be in a "
                             ".tsv format.")
    parser.add_argument("-model_path",
                        default="models/default_model",
                        type=str,
                        help="The path where model weights and architecture are stored.")
    parser.add_argument("-data_column",
                        default="tweet",
                        type=str,
                        help="Name of the column containing the data in the provided dataset.\n"
                             "If you are using the pretrained model, make sure your data column is\n"
                             "named 'tweet'.\nIf you are using your own trained model, the name of the\n"
                             "data column should be the same as it was during training.")
    parser.add_argument("-class_labels",
                        default=['TIN', 'UNT'],
                        type=str,
                        nargs="+",
                        help="Class labels. If you are using a model you trained yourself, please insert "
                             "class labels in the same order you inserted them during training.")

    args = parser.parse_args()

    current_dir = os.path.dirname(os.path.abspath(__file__))
    if args.data_path == "testData_subtaskB/testset-taskb.tsv":
        test_file = os.path.join(current_dir, args.data_path)
    else:
        test_file = args.data_path

    try:
        x_test = pd.read_csv(test_file, delimiter="\t")
    except FileNotFoundError:
        print("Error: File missing.")
        print("The path to the dataset you provided is invalid.\n"
              "Please make sure you entered the correct path.")
        sys.exit()

    #print(x_test)

    #ids = x_test['id'].tolist()
    data_column = args.data_column
    try:
        tweets = x_test[data_column].tolist()
        print(tweets)
    except KeyError:
        print("Error: column not found.")
        print("The column with the name '%s' was not found in the dataset." % data_column)
        sys.exit()
    
    print("Creating tfidf matrices for input data.")
    if args.model_path == "models/default_model":
        model_path = os.path.join(current_dir, args.model_path)
    else:
        model_path = args.model_path

    tfidf_path = os.path.join(model_path, "tfidf_vectorizer")

    try:
        tfidf = pickle.load(open(tfidf_path, 'rb'))
    except IOError:
        print("Error: file not found.")
        print("Please check the path to the model weights and architecture files.")
        sys.exit()

    tfidf_xtest = tfidf.transform(x_test)
    print(tfidf_xtest)
    tfidf_xtest = build_features(tfidf_xtest, x_test, data_column)
    print(tfidf_xtest.shape[1])
    #model = build_model

    print("Creating a numerical representation of input data")

    keras_tokenizer_path = os.path.join(model_path, "keras_tokenizer")
    try:
        keras_tokenizer = pickle.load(open(keras_tokenizer_path, 'rb'))
    except IOError:
        print("Error: file not found.")
        print("Please check the path to the model weights and architecture files.")
        sys.exit()


    model_arch_path = os.path.join(model_path, "model_arch_dim")
    try:
        model_arch = pickle.load(open(model_arch_path, 'rb'))
    except IOError:
        print("Error: file not found.")
        print("Please check the path to the model weights and architecture files.")
        sys.exit()

    tfidf_matrix_train_shape, max_train_len, vocab_size, num_classes, embedding_matrix, \
        embedding_dim = model_arch
    
    xtest_numeric_tokens = keras_tokenizer.texts_to_sequences(x_test[data_column].tolist())
    xtest_vec = pad_sequences(xtest_numeric_tokens, max_train_len)
    print(xtest_vec)

    print("Building the model")
    model = build_model(tfidf_matrix_train_shape, max_train_len, vocab_size, num_classes, embedding_matrix,
                        embedding_dim)
    weights_path = os.path.join(model_path, "model_weights")
    try:
        model.load_weights('{}.hdf5'.format(weights_path))
    except IOError:
        print("Error: file not found.")
        print("Please check the path to the model weights and architecture files.")
        sys.exit()

    print("Outputting predictions")
    output = model.predict([tfidf_xtest, xtest_vec], batch_size=16)
    preds = np.argmax(output, axis=1).tolist()
    print(preds)

    preds_class = []
    class_labels = args.class_labels
    for x in preds:
        if x == 0:
            preds_class.append(class_labels[0])
        else:
            preds_class.append(class_labels[1])

    with open('preds.tsv', 'w', encoding='utf8') as f:
        for tweet, pred in zip(tweets, preds_class):
            f.write(str(tweet) + '\t' + str(pred) + "\n")


if __name__ == '__main__':
    predict()
