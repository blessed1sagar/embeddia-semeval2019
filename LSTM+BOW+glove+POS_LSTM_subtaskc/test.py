#Author: Andraž P.

from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize.treebank import TreebankWordTokenizer
import os
import pandas as pd
import copy
from features import sentiment_request, encode_sentiment_labels, remove_quotes, pos_tagging, build_pos_vocab
import numpy as np

def stem_data_test():
    file = os.path.join(os.path.dirname(__file__), "test.csv")
    xtrain = pd.read_csv(file)
    xtrain_tokenized = copy.deepcopy(xtrain)

    print("Original sentences:")
    print(xtrain)
    print("Copied sentences:")
    print(xtrain_tokenized)

    treebank_tokenizer_object = TreebankWordTokenizer()
    stemmer = SnowballStemmer("english")

    for index, row in xtrain_tokenized.iterrows():
        tokens = treebank_tokenizer_object.tokenize(row['tweet'])
        print(tokens)
        stemmed = [stemmer.stem(token) for token in tokens]
        print(stemmed)
        row['tweet'] = stemmed

    print("Original sentences:")
    print(xtrain)
    print("Stemmed sentences:")
    print(xtrain_tokenized)


def encode_sentiment_label_test():
    path = os.path.join(os.path.dirname(__file__), os.pardir)
    train_file = "/training-v1/offenseval-training-v1-train_split.tsv"
    train_data = pd.read_csv(path + train_file, sep="\t")
    response = sentiment_request(train_data, 'tweet')
    encoded_sentiment = encode_sentiment_labels(response)
    sentiment_vector = np.array(encoded_sentiment)
    print(sentiment_vector)
    for sentiment in sentiment_vector:
        assert((sentiment >= 0) and (sentiment < 4))


def POS_tagging_test():
    file = os.path.join(os.path.dirname(__file__), "test.csv")
    xtrain = pd.read_csv(file)

    list_of_tags = pos_tagging(xtrain['tweet'].tolist())
    print(list_of_tags)
    pos_vocab = build_pos_vocab(list_of_tags)
    print(pos_vocab)



if __name__ == '__main__':
    POS_tagging_test()
