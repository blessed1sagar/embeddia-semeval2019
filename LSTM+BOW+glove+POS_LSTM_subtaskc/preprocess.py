#Author: Andraž P.

import numpy as np


def preprocess_data(df_data, target):
    """"Divides the dataset into data and labels
        INPUTS: df_data - dataframe of the original dataset
                target - the name of the column containing class labels
        OUTPUTS: xdata - dataframe containing only data of the original dataset
                 ydata - dataframe containing only labels of the original dataset
    """
    if target not in df_data.columns.values.tolist():
        raise ValueError
    xdata = df_data.drop([target], axis=1)
    ydata = df_data[target]
    print("Examples:")
    print(xdata)
    print("Shape of data matrix:")
    print(xdata.shape)
    print("Labels:")
    print(ydata)
    return xdata, ydata


def tags_to_idx(labels, idx):
    """Encodes labels into numerical values. Each label is associated with its index in the list.
        INPUTS: labels - dataframe of labels for each instance in the dataset
                idx - list of possible labels (type string)
        OUTPUT: y - list of encoded labels
    """
    #idx = ['IND', 'GRP', 'OTH']
    #if len(idx) < 1:
    #    idx = list(set(labels.tolist()))
    y = np.array([idx.index(tmp_y) for tmp_y in labels.tolist()])
    print("Encoded labels:")
    print(y)
    print("Shape of the labels vector:")
    print(y.shape)
    #y = y[:, None]
    print(y.shape)
    return y


def get_vocab_size(train_data):
    """Vrne število različnih besed v korpusu"""
    max_index_per_tweet = [max(tweet) for tweet in train_data]
    vocab_size = max(max_index_per_tweet)
    return vocab_size

