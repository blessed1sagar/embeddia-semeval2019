#Author: Andraž P.

from preprocess import preprocess_data, tags_to_idx, get_vocab_size
import pandas as pd
import os
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from model import build_model
import gc
from keras import backend as K
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint
from nltk.corpus import stopwords
from sklearn import pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
import pickle
from features import build_features, prepare_embedding_matrix, get_max_text_len, pos_tagging, build_pos_vocab, make_pos_vec
from nltk.tokenize import TweetTokenizer
import numpy as np
import argparse
import sys
#from nltk.tokenize.treebank import TreebankWordTokenizer


class text_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]


def run():
    """Starts the training and validation of the model."""

    parser = argparse.ArgumentParser()

    parser.add_argument("-train_set_path",
                        type=str,
                        help="The path to the training dataset. The dataset should be in a \
                        .tsv format.",
                        required=True)
    parser.add_argument("-validation_set_path",
                        type=str,
                        help="The path to the validation dataset. The dataset should be in a \
                        .tsv format.",
                        required=True)
    parser.add_argument("-model_path",
                        default="models/default_model",
                        type=str,
                        help="The path where model weights and architecture will be saved.")
    parser.add_argument("-label_column",
                        type=str,
                        help="Name of the column containing the label in the provided dataset.",
                        required=True)
    parser.add_argument("-data_column",
                        type=str,
                        help="Name of the column containing the data in the provided dataset.",
                        required=True)
    parser.add_argument("-num_classes",
                        default=3,
                        type=int,
                        help="Number of classes for prediction.")
    parser.add_argument("-class_labels",
                        default=['IND', 'GRP', 'OTH'],
                        type=str,
                        nargs="+",
                        help="Class labels.")

    args = parser.parse_args()

    current_folder = os.path.dirname(os.path.abspath(__file__))
    if args.model_path == "models/default_model":
        model_path = os.path.join(current_folder, args.model_path)
    else:
        model_path = args.model_path

    try:
        if os.listdir(model_path):
            print("Error: the specified directory is not empty.")
            print("Please specify an empty directory in order to avoid rewriting "
                  "model weights\nand architecture.")
            sys.exit()
    except FileNotFoundError:
        print("Error: File not found")
        print("The path to the model weights and architecture you provided is invalid.")
        print("Please make sure you entered the correct path.")
        sys.exit()

    model_weights_file = os.path.join(model_path, 'model_weights.hdf5')
    checkpointer = ModelCheckpoint(filepath=model_weights_file,
                                   verbose=1,
                                   monitor="val_acc",
                                   save_best_only=True,
                                   mode="max")

    num_classes = args.num_classes
    class_labels = args.class_labels

    if len(class_labels) is not num_classes:
        print("Error: incorrect number of labels.")
        print("Number of class labels provided must be the same as the number of classes provided.")
        sys.exit()

    train_path = args.train_set_path
    val_path = args.validation_set_path

    try:
        train_data = pd.read_csv(train_path, sep="\t")
    except FileNotFoundError:
        print("Error: File not found.\n")
        print("The path to the training dataset you provided is invalid.")
        print("Please make sure you entered the correct path.")
        sys.exit()

    try:
        val_data = pd.read_csv(val_path, sep="\t")
    except FileNotFoundError:
        print("Error: File not found.\n")
        print("The path to the validation dataset you provided is invalid.")
        print("Please make sure you entered the correct path.")
        sys.exit()

    print("Preprocessing data")
    label = args.label_column
    try:
        xtrain, ytrain = preprocess_data(train_data, label)
    except ValueError:
        print("Error: Column not found.")
        print("The column with the name '%s' was not found in the training dataset." % label)
        sys.exit()
    try:
        ytrain = tags_to_idx(ytrain, class_labels)
    except ValueError:
        print("Error: Invalid label.")
        print("There was an unknown class label found in the dataset.")
        print("Please provide all possible class labels at the program initialization.")
        sys.exit()
    ytrain_categorical = to_categorical(ytrain, num_classes=num_classes)
    print("ytrain hot encoded")
    print(ytrain)

    try:
        xval, yval = preprocess_data(val_data, label)
    except ValueError:
        print("Error: Column not found.")
        print("The column with the name '%s' was not found in the validation dataset." % label)
        sys.exit()
    try:
        yval = tags_to_idx(yval, class_labels)
    except ValueError:
        print("Error: Invalid label.")
        print("There was an unknown class label found in the dataset.")
        print("Please provide all possible class labels at the program initialization.")
        sys.exit()
    yval_categorical = to_categorical(yval, num_classes=num_classes)
    print("yval hot-encoded")
    print(yval_categorical)

    data_column = args.data_column
    if data_column not in xtrain.columns.values.tolist():
        print("Error: Column not found")
        print("The column with the name '%s' was not found in the training dataset." % data_column)
        sys.exit()

    if data_column not in xval.columns.values.tolist():
        print("Error: Column not found")
        print("The column with the name '%s' was not found in the validation dataset." % data_column)
        sys.exit()

    print("Creating a POS sequence numerical representation of input data")
    max_train_len = get_max_text_len(xtrain, data_column)
    print("Max text length:")
    print(max_train_len)
    print(xtrain[data_column].tolist())
    
    xtrain_pos_tags = pos_tagging(xtrain[data_column].tolist())
    pos_vocab = build_pos_vocab(xtrain_pos_tags)
    pos_vocab_size = len(pos_vocab) + 2
    posvec_train = make_pos_vec(xtrain_pos_tags, pos_vocab, max_train_len)
    xval_pos_tags = pos_tagging(xval[data_column].tolist())
    posvec_val = make_pos_vec(xval_pos_tags, pos_vocab, max_train_len)

    print("Creating tfidf matrices for input data.")
    stopwords_list = set(stopwords.words('english'))

    word_ngrams = pipeline.Pipeline([
        ('selector', text_col(key=data_column)),
        ('word_vectorizer', CountVectorizer(analyzer='word', ngram_range=(1, 5), lowercase=True,
                                            stop_words=stopwords_list, min_df=5)),
        ('tfidf_word', TfidfTransformer(sublinear_tf=True))
        ])

    char_ngrams = pipeline.Pipeline([
        ('selector', text_col(key=data_column)),
        ('char_vectorizer', CountVectorizer(analyzer='char', ngram_range=(1, 7), lowercase=True,
                                            min_df=5)),
        ('tfidf_char', TfidfTransformer(sublinear_tf=True))
        ])

    features = FeatureUnion([('word', word_ngrams),
                             ('char', char_ngrams)])
    
    tfidf = pipeline.Pipeline([('features', features)])
    tfidf.fit(xtrain)

    tfidf_file = os.path.join(model_path, "tfidf_vectorizer")
    print(tfidf_file)
    with open(tfidf_file, 'wb') as f:
        pickle.dump(tfidf, f, protocol=2)

    tfidf_matrix_train = tfidf.transform(xtrain)
    tfidf_matrix_train = build_features(tfidf_matrix_train, xtrain, data_column)
    tfidf_matrix_train_shape = tfidf_matrix_train.shape[1]

    tfidf_matrix_val = tfidf.transform(xval)
    tfidf_matrix_val = build_features(tfidf_matrix_val, xval, data_column)
    
    print("Creating a word sequence numerical representation of input data")
    tokenizer_object = Tokenizer(lower=True)
    tokenizer_object.fit_on_texts(xtrain[data_column].tolist())

    tokenizer_file = os.path.join(model_path, "keras_tokenizer")
    with open(tokenizer_file, 'wb') as handler:
        pickle.dump(tokenizer_object, handler, protocol=2)

    xtrain_numeric_tokens = tokenizer_object.texts_to_sequences(xtrain[data_column].tolist())
    #vocab_size = get_vocab_size(xtrain_numeric_tokens)
    word_index = tokenizer_object.word_index
    vocab_size = len(word_index)
    print("Vocabulary size")
    print(len(word_index))
    xval_numeric_tokens = tokenizer_object.texts_to_sequences(xval[data_column].tolist())

    xtrain_vec = pad_sequences(xtrain_numeric_tokens, max_train_len)
    xval_vec = pad_sequences(xval_numeric_tokens, max_train_len)

    print("Preparing the embedding matrix")
    path = os.path.join(os.path.dirname(__file__), os.pardir)
    embedding_file = os.path.join(path, "glove.twitter.27B.100d.txt")
    embedding_dim = 100
    embedding_matrix = prepare_embedding_matrix(embedding_file, embedding_dim, word_index)

    print("Building the model")
    num_epoch = 10
    model = build_model(tfidf_matrix_train_shape, max_train_len, vocab_size, num_classes,
                        embedding_matrix, embedding_dim, pos_vocab_size)

    model_archi_dim = tfidf_matrix_train_shape, max_train_len, vocab_size, num_classes, \
                      embedding_matrix, embedding_dim, pos_vocab_size, pos_vocab
    model_archi_dim_file = os.path.join(model_path, "model_arch_dim")
    with open(model_archi_dim_file, 'wb') as model_file:
        pickle.dump(model_archi_dim, model_file, protocol=2)

    print("Training the model")
    model.fit([tfidf_matrix_train, xtrain_vec, posvec_train], ytrain,
              validation_data=([tfidf_matrix_val, xval_vec, posvec_val], yval), batch_size=16,
              epochs=num_epoch, callbacks=[checkpointer], verbose=2)

    K.clear_session()
    gc.collect()
    validate_model()
    print("Done")


def validate_model(tfidf_matrix_val, xval_vec, posvec_val, yval, model_path):

    model_arch_path = os.path.join(model_path, "model_arch_dim")
    model_arch = pickle.load(open(model_arch_path, 'rb'))
    tfidf_matrix_train_shape, max_train_len, vocab_size, num_classes, embedding_matrix, \
        embedding_dim, pos_vocab_size, pos_vocab = model_arch

    print("Building the model")
    weights_path = os.path.join(model_path, "model_weights")
    model = build_model(tfidf_matrix_train_shape, max_train_len, vocab_size, num_classes,
                        embedding_matrix, embedding_dim, pos_vocab_size)
    model.load_weights('{}.hdf5'.format(weights_path))

    print("Outputting predictions")
    output = model.predict([tfidf_matrix_val, xval_vec, posvec_val], batch_size=16)
    preds = np.argmax(output, axis=1).tolist()
    print(preds)

    f1 = f1_score(yval, preds, average='macro')
    print("F1 score: %f" % f1)


if __name__ == '__main__':
    validate_model()
