**Installation**

Install python 3.6, create a virtual environment and install the required dependencies from the file 'requirements.txt'.
The trained model files are relatively large and are stored as LFS pointers. You can download them using git-lfs tool. For more information on how to use this tool, check the following tutorial: https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html

**Additional resources**

The scripts for models for subtask b and c need access to 100d GloVe embeddings pretrained on twitter data. Please download them separately from the following URL: http://nlp.stanford.edu/data/glove.twitter.27B.zip. Then extract the file 'gloe.twitter.27B.100d.txt' to the root folder of the repository.

**How to use the models**

Every model has two main scripts, 'predict.py' and 'train.py'.
'predict.py' enables you to use the models that were trained by us and reported in the article.
'train.py' enables you to use the architecture to train your own models on your own data.
Note that the scripts require you to have your data stored in a .tsv format.

**Parameter description**

BERT\_subtask_a:

*'predict.py'*:

There are four parameters that need to be set to effectively use the model for the subtask_a:
	- '--data_path': The path of the input data file. The data should be in the .tsv format with 			the following structure: column 1 - ID, column 2: data for classification.
	- '--task_name': the name of the tash, should be kept a a default value of SEMEVAL
	- '--output_file': the path to the file where the model will print the results of 			classification
	- '--model\_path': path to the trained model, shold be kept as-is. Change this parameter only 			if you move the model from its original directory 					('BERT\_subtask\_a/semeval\_output\_task_a )

*'train.py'*:

The script is based on the original example script for fine-tunning BERT from the pytorch-pretrained-bert repository at URL 'https://github.com/wanghm92/pytorch-pretrained-BERT'. For training the model with the same parameters as they were reported in our article, simply set the path to your train and evaluation data with the paramaters '--train\_set_path' and '--validation\_set\_path'. The data should be in the .tsv format with the following structure: column 1 - training data, column 2: label. The labels should be 'OFF' and 'NOT' as this mimics the original training setting for which this script was used. If you want to train this model for other data, refer to the original repository for the usage example or contact me.


LSTM+BOW+glove_subtaskb:

*'predict.py'*:

	- '-data_path': The path to the data instances to be clasified. The dataset should be in 			a .tsv format.
	- '-model_path': The path where trained model weights and architecture are stored.
	- '-data_column': Name of the column containing the data in the provided dataset. If you are 			using the pretrained model, make sure your data column is named 'tweet'. If you are 			using your own trained model, the name of the data column should be the same as it 			was during training.
	- '-class_labels': Class labels. If you are using our trained model for prediction, the class 			labels are already set. If you are using a model you trained yourself, please insert 			class labels in the same order you inserted them during training.

*'train.py'*:

	- '-train\_set_path': The path to the training dataset. The dataset should be in a .tsv 		format.
	- '-validation\_set_path': The path to the validation dataset. The dataset should be in 		a .tsv format.
	- '-model_path': The path where model weights and architecture will be saved. Default 			path: models/default model
	- '-label_column': Name of the column containing the label in the provided datasets.
	- '-data_column': Name of the column containing the data in the proviced datasets.
	- '-num_classes': Number of classes. Default value 2.
	- '-class_labels': Class labels. The default class labels mimic the original training setting.

LSTM+BOW+glove+POS_LSTM_subtaskc:

*'predict.py'*:
	
	- 'data_path': The path to the data instances to be clasified. The dataset should be in 		a .tsv format.
	- 'model_path': The path where trained model weights and architecture are stored.
	- 'data_column': Name of the column containing the data in the provided dataset. If you are 			using the pretrained model, make sure your data column is named 'tweet'. If you are 			using your own trained model, the name of the data column should be the same as it 			was during training.
	- 'class_labels': Class labels. If you are using our trained model for prediction, the class 			labels are already set. If you are using a model you trained yourself, please insert 			class labels in the same order you inserted them during training.

*'train.py'*:

	- '-train\_set_path': The path to the training dataset. The dataset should be in a .tsv 		format.
	- '-validation\_set_path': The path to the validation dataset. The dataset should be in 		a .tsv format.
	- '-model_path': The path where model weights and architecture will be saved. Default 			path: models/default model
	- '-label_column': Name of the column containing the label in the provided datasets.
	- '-data_column': Name of the column containing the data in the proviced datasets.
	- '-num_classes': Number of classes. Default value 3.
	- '-class_labels': Class labels. The default class labels mimic the original training setting.

You can use the code for your own scientific purposes and change it based on your needs. You can give recognition for our work by citing our article:

@inproceedings{pelicon2019embeddia,
  title={Embeddia at SemEval-2019 Task 6: Detecting Hate with Neural Network and Transfer Learning Approaches},
  author={Pelicon, Andra{\v{z}} and Martinc, Matej and Novak, Petra Kralj},
  booktitle={Proceedings of the 13th International Workshop on Semantic Evaluation},
  pages={604--610},
  year={2019}
}

If you need any further assisstance, contact me at Andraz.Pelicon@ijs.si.
